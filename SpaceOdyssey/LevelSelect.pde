public class LevelSelect {
  int selection;
  boolean menukey;
  PImage bg;

  public LevelSelect() {
    bg = loadImage("Assets/Visual/Backgrounds/SpaceBackgroundPixel.jpg");
    selection = 0;
    menukey = true;
  }

  void display() {
    background(bg);
    textSize(100);
    fill(255, 255, 255);
    text("Level Select", 100, 100);

    if (selection == 0) {
      fill(204, 78, 22);
      textSize(60);
      text("Level 1", 150, 200);
      fill(255, 255, 255);
      textSize(60);
      text("Level 2", 300, 300);
      fill(255, 255, 255);
      textSize(60);
      text("Level 3", 420, 400);
      fill(255, 255, 255);
      textSize(60);
      text("Main Menu", 500, 530);
    }
    if (selection == 1) {
      fill(255, 255, 255);
      textSize(60);
      text("Level 1", 150, 200);
      fill(204, 78, 22);
      textSize(60);
      text("Level 2", 300, 300);
      fill(255, 255, 255);
      textSize(60);
      text("Level 3", 420, 400);
      fill(255, 255, 255);
      textSize(60);
      text("Main Menu", 500, 530);
    }
    if (selection == 2) {
      fill(255, 255, 255);
      textSize(60);
      text("Level 1", 150, 200);
      fill(255, 255, 255);
      textSize(60);
      text("Level 2", 300, 300);
      fill(204, 78, 22);
      textSize(60);
      text("Level 3", 420, 400);
      fill(255, 255, 255);
      textSize(60);
      text("Main Menu", 500, 530);
    }
    if (selection == 3) {
      fill(255, 255, 255);
      textSize(60);
      text("Level 1", 150, 200);
      fill(255, 255, 255);
      textSize(60);
      text("Level 2", 300, 300);
      fill(255, 255, 255);
      textSize(60);
      text("Level 3", 420, 400);
      fill(204, 78, 22);
      textSize(60);
      text("Main Menu", 500, 530);
    }
  }

  void keyPressed() {
    if (keyCode == UP) {
      if (selection > 0) {
        selection -= 1;
        menukey = false;
        reproductor.select.trigger();
      }
    }

    if (keyCode == DOWN) {
      if (selection < 3) {
        selection += 1;
        menukey = false;
        reproductor.select.trigger();
      }
    }

    if (keyCode == ENTER) {
      reproductor.pressEnter.trigger();
      if (selection == 0) {
        start = true;
        selectedlevel = 1;
        level[selectedlevel-1].resetLevel();
      }
      if (selection == 1) {
        start = true;
        selectedlevel = 2;
        level[selectedlevel-1].resetLevel();
      }
      if (selection == 2) {
        start = true;
        selectedlevel = 3;
        level[selectedlevel-1].resetLevel();
      }
      if (selection == 3) {
        menu = -1;
      }
    }
  }

  void keyReleased() {
    if (keyCode == UP) {
      menukey = true;
    }
    if (keyCode == DOWN) {
      menukey = true;
    }
    if (keyCode == ENTER) {
      menukey = true;
    }
  }
}