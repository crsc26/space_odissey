public class Options {
  int selection;
  float gain;
  int aux;
  int aux1;
  boolean opkey;
  boolean mute;
  PImage bg;


  public Options () {
    bg = loadImage("Assets/Visual/Backgrounds/SpaceBackgroundPixel.jpg");
    gain = 0;
    selection = 0;
    opkey = true;
    aux=0;
    aux1=0;
  }

  void display() {
    background(bg);
    textSize(100);
    fill(255, 255, 255);
    text("Options", 100, 100);

    if (selection == 0) {
      fill(204, 78, 22);
      textSize(60);
      text("Volume", 300, 200);
      text (volume, 550, 200);
      fill(255, 255, 255);
      textSize(60);
      text("Main Menu", 380, 300);
      fill(255, 255, 255);
      textSize(60);
      text("Mute", 40, 550);
    }
    if (selection == 1) {
      fill(255, 255, 255);
      textSize(60);
      text("Volume", 300, 200);
      fill(204, 78, 22);
      textSize(60);
      text("Main Menu", 380, 300);
      fill(255, 255, 255);
      textSize(60);
      text("Mute", 40, 550);
    }
    if (selection == 2) {
      fill(255, 255, 255);
      textSize(60);
      text("Volume", 300, 200);
      fill(255, 255, 255);
      textSize(60);
      text("Main Menu", 380, 300);
      fill(204, 78, 22);
      textSize(60);
      text("Mute", 40, 550);
      text(volume, 220, 550);
    }
  }

  void keyPressed() {
    if (keyCode == UP) {
      if (selection > 0) {
        selection -= 1;
        opkey = false;
        reproductor.select.trigger();
      }
    }

    if (keyCode == DOWN) {
      if (selection < 2) {
        selection += 1;
        opkey = false;
        reproductor.select.trigger();
      }
    }

    if (keyCode == ENTER) {
      reproductor.pressEnter.trigger();
      if (selection == 1) {
        menu = -1;
      }
    }
    if (keyCode==LEFT) {
      if (selection==0&&volume>0) {
        volume=volume-20;
        reproductor.setVol(volume);
      }
    }
    if (keyCode==RIGHT&&volume<100) {
      if (selection==0) {
        volume=volume+20;
        reproductor.setVol(volume);
      }
    }

    if (keyCode==LEFT&&selection==2) {
      if (!mute) {
        aux=volume;
        volume=0;
        reproductor.muteAll();
        mute = true;
      }
    }

    if (keyCode==RIGHT&&selection==2) {
      if (mute) {
        volume=aux;
        reproductor.unmuteAll();
        mute = false;
      }
    }
  }

  void keyReleased() {
    if (keyCode == UP) {
      opkey = true;
    }
    if (keyCode == LEFT) {
      opkey = true;
    }
    if (keyCode == RIGHT) {
      opkey = true;
    }
    if (keyCode == DOWN) {
      opkey = true;
    }
    if (keyCode == ENTER) {
      opkey = true;
    }
  }
}