public class Pause {
  boolean pausekey;
  int selection;

  public Pause () {
    pausekey = true;
    selection = 0;
  }

  void display() {
    textSize(100);
    fill(255, 255, 255);
    text("Pause", 100, 140);
    if (selection == 0) {
      fill(204, 78, 22);
      textSize(30);
      text("Continue", 2*width/3, height - 80);
      fill(255, 255, 255);
      textSize(30);
      text("Main Menu", 2*width/3, height - 40);
    }
    if (selection == 1) {
      fill(255, 255, 255);
      textSize(30);
      text("Continue", 2*width/3, height - 80);
      fill(204, 78, 22);
      textSize(30);
      text("Main Menu", 2*width/3, height - 40);
    }
  }

  void keyPressed() {
    if (pausekey) {
      if (keyCode == UP) {
        if (selection > 0) {
          reproductor.select.trigger();
          selection -= 1;
          pausekey = false;
        }
      }

      if (keyCode == DOWN) {
        if (selection < 1) {
          reproductor.select.trigger();
          selection += 1;
          pausekey = false;
        }
      }

      if (key == 'p') {
        pause = false;
        pausekey = false;
      }

      if (keyCode == ENTER) {
        reproductor.pressEnter.trigger();
        if (selection == 0) {
          pause = false;
        }
        if (selection == 1) {
          reproductor.gameMenuSong.rewind();
          menu = -1;
          start = false;
          pause = false;
        }
      }
    }
  }

  void keyReleased() {
    if (keyCode == UP) {
      pausekey = true;
    }
    if (keyCode == DOWN) {
      pausekey = true;
    }
    if (keyCode == ENTER) {
      pausekey = true;
    }
  }
}