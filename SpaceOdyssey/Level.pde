public class Level {
  int NBR_M;
  int NBR_P;
  int NBR_PU;
  int pucounter;
  int counterM;

  float auxplus, auxminus;
  PImage bg, bg2;
  float map_x1, map_x2;
  int map_xspeed, mapx_count;
  float duration;
  Player player;
  Meteorite[] m;
  Planet[] p;
  PowerUp[] pu;

  public Level() {
    bg = loadImage("Assets/Visual/Backgrounds/space-bg2.png");
    bg2 = loadImage("Assets/Visual/Backgrounds/space-bg2.png");
    score = 0;
    map_xspeed = 12;
    map_x1 = 0;
    map_x2 = width + (bg.width - width - 10);
    mapx_count = 0;
    m = new Meteorite[NBR_M];
    p = new Planet[NBR_P];
    pu = new PowerUp[NBR_PU];
    player = new Player();
    auxplus = player.plusaccel;
    auxminus = player.minusaccel;
    for (int i = 0; i < NBR_M; i++) {
      m[i] = new Meteorite();
    }
    for (int i = 0; i < NBR_PU; i++) {
      pu[i] = new PowerUp(1);
    }
  }

  void draw() {
    meteorResetCheck(NBR_M);

    levelFinishCheck();

    bgDisplay();

    playerDisplay();

    actorCollisionCheck(NBR_M);
    playerBoundaryCheck();
    mapUpdate();
  }

  void playerDisplay() {
    //Display del jugador
    player.update();
    player.display();
  }

  void bgDisplay() {
    //Display del Fondo
    set((int)map_x1, 0, bg);
    set((int)map_x2, 0, bg2);
  }

  void meteorResetCheck(int n) {
    //Reset de los meteoritos al salir de la pantalla
    for (int i = 0; i < n; i++) {
      if (m[i].xpos < -5 || m[i].ypos > height+5 || m[i].ypos < -5) {
        m[i].reset();
      }
    }
  }

  void gravCollision(int n, Planet[] p) {
    for (int i = 0; i < n; i++) {
      if (p[i].grav.checkCollision(player.col)) {
        if (p[i].type == 1) {
          player.acceleration += +0.03;
        } else {
          player.acceleration += -0.03;
        }
      } else {
        player.plusaccel = auxplus;
        player.minusaccel = auxminus;
      }
    }
  }

  void planetCollisionCheck(int n, Planet[] p) {
    for (int i = 0; i < n; i++) {
      if (p[i].col.checkCollision(player.col)) {
        reproductor.crash.trigger();
        score = (int)((mapx_count/(duration)*100));
        gameover =  true;
      }
    }
  }

  void actorCollisionCheck(int n) {
    //Chequeo de Colisiones
    for (int i = 0; i < n; i++) {
      if (m[i].col.checkCollision(player.col)) {
        reproductor.crash.trigger();
        score = (int)((mapx_count/(duration)*100));
        gameover =  true;
      }
    }
  }

  // PowerUpCollision
  /* Estados:
   0: Ready
   1: Moving up
   2: Moving down
   3: Pause
   4: Victory
   5: Untouchable
   6: Small
   7: Invert controls  
   */

  void powerUpCollision (int n)
  {
    for (int i = 0; i < n; i++)
    {
      if (pu[i].col.checkCollision(player.col))
      {
        switch (pu[i].type)
        {
        case 1:
          pu[i].powerUpDisappear();
          player.state = 5;
          player.power = 5;
          pucounter = 0;
          break;

        case 2:
          pu[i].powerUpDisappear();
          player.state = 8;
          player.power = 6;
          pucounter = 0;
          player.changeSize();
          break;

        case 3:
          pu[i].powerUpDisappear();
          player.state = 11;
          player.power = 7;
          pucounter = 0;
          break;
        }
      }
    }
  }

  void powerUpTime(int n, int i) {
    if (pucounter % 30 < 15)
      pu[i].activePowerDisplay();
    pucounter += 1;
    if (pucounter > n*30) {
      pucounter = 0;
      if (player.power == 6)
        player.revertSize();
      if (player.power == 7)
        player.state = 0;
      player.power = 0;
    }
  }

  // PowerUpEffect

  /*
  void PowerUpEffect ()
   {
   if (player.state == // el número que elija Rodri)
   {
   pucounter = 1
   }
   
   if (pucounter = 11231231)
   {
   player.state == // el número que elija Rodri
   pucounter = 0;
   }
   
   if (player.state == // el número que elija Rodri
   {
   // Invertir controles
   }
   
   }
   */



  void playerBoundaryCheck() {
    //GameOver por salirse del mapa
    if (player.ypos < -60 || player.ypos > height+30) {
      reproductor.flyaway.trigger();
      score = (int)((mapx_count/duration)*100);
      gameover =  true;
    }
  }

  void mapUpdate() {
    //Ciclo de repeticion del mapa
    if (map_x1 <= -bg.width) {
      map_x1 =  width + (bg2.width - width - 10);
    }
    if (map_x2 <= -bg2.width) {
      map_x2 =  width + (bg.width - width - 10);
    }

    //Movimiento del Mapa
    map_x1 += -(map_xspeed);
    map_x2 += -(map_xspeed);
    if (mapx_count < duration) {
      mapx_count += map_xspeed;
      score = mapx_count/2;
    }
  }

  boolean levelFinishCheck() {
    if (player.xpos>width+36) {
      reproductor.shutUp();
      reproductor.victorySong.play();
      overscreen.selection = 0;
      victory = true;
      return true;
    } else {
      return false;
    }
  }

  void progressText() {
    textSize(20);
    text("Level Completion: " + (int)((score/(duration/2)*100))+ "%", 795, 30);//PORCENTAJE DEL NIVEL
  }

  void finishAnimation() {
    if (mapx_count >= duration) {
      player.state = 4;
      player.velocity = 0;
      player.acceleration = 0;
      player.xpos += 11;
    }
  }

  void progressbar() {
    if (score<((duration/2)-900)) {
      fill(255);
      //Duration/(width de la pantalla*2)
      rect(0, 567, score/(duration/(width*2)), 8);
    } else if (score>9500) {
      fill(211, 110, 112);
      rect(0, 567, score/(duration/(width*2)), 8);
    }
  }

  void meteorDisplay(int n) {
    for (int i = 0; i < n; i++) {
      m[i].update();
      m[i].display();
    }
  }

  void powerupDisplay(int n) {
    for (int i = 0; i < n; i++) {
      pu[i].update();
      pu[i].display();
    }
  }

  /*
  //Display de Meteoritos
   for (int i = 0; i < NBR_M; i++) {
   m[i].update();
   m[i].display();
   }
   */
  void resetLevel() {
    player.state = 0;
    map_x1 = 0;
    map_x2 = width + (bg.width - width - 10);
    map_x1 = 0;
    map_x2 = width + (bg.width - width - 10);
    mapx_count = 0;
    player = new Player();
    for (int i = 0; i < NBR_M; i++) {
      m[i] = new Meteorite();
    }
    for (int i = 0; i < NBR_PU; i++) {
      pu[i] = new PowerUp(1);
    }
  }

  void keyPressed() {
    if (keyCode == UP) {
      if (mapx_count < duration) {
        if (player.power == 7) {
          player.acceleration -= player.plusaccel;
        } else {
          player.acceleration += player.plusaccel;
        }
        player.acceleration = constrain(player.acceleration, -1, 1);
        switch (player.power) {
        case 0:
          player.state = 1;
          break;
        case 5:
          player.state = 6;
          break;
        case 6:
          player.state = 9;
          break;
        case 7:
          player.state = 12;
          break;
        }
      }
    }

    if (keyCode == DOWN) {
      if (mapx_count < duration) {
        if (player.power == 7) {
          player.acceleration -= player.minusaccel;
        } else {
          player.acceleration += player.minusaccel;
        }
        player.acceleration = constrain(player.acceleration, -1, 1);
        switch (player.power) {
        case 0:
          player.state = 2;
          break;
        case 5:
          player.state = 7;
          break;
        case 6:
          player.state = 10;
          break;
        case 7:
          player.state = 13;
          break;
        }
      }
    }
    if (key == 'p') {
      if (mapx_count < duration) {
        player.state = 3;
        player.display();
        reproductor.shutUp();
        reproductor.gamePauseSong.play();
        pause = true;
      }
    }
  }

  void keyReleased() {
    if (keyCode == UP) {
      if (mapx_count < duration) {
        switch (player.power) {
        case 0:
          player.state = 0;
          break;
        case 5:
          player.state = 5;
          break;
        case 6:
          player.state = 8;
          break;
        case 7:
          player.state = 11;
          break;
        }
      }
    }
    if (keyCode == DOWN) {
      if (mapx_count < duration) {
        switch (player.power) {
        case 0:
          player.state = 0;
          break;
        case 5:
          player.state = 5;
          break;
        case 6:
          player.state = 8;
          break;
        case 7:
          player.state = 11;
          break;
        }
      }
    }
    if (key == 'p') {
    }
  }
}