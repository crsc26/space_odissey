public class Level1 extends Level {
  int NBR_M = 2;
  int NBR_P = 0;

  public Level1() {
    bg = loadImage("Assets/Visual/Backgrounds/space-bg2.png");
    bg2 = loadImage("Assets/Visual/Backgrounds/space-bg2.png");
    score = 0;
    map_xspeed = 12;
    map_x1 = 0;
    map_x2 = width + (bg.width - width - 10);
    duration = 21600;  //1 minuto
    mapx_count = 0;
    counterM = 0;
    m = new Meteorite[NBR_M];
    for (int i = 0; i < NBR_M; i++) {
      m[i] = new Meteorite();
    }
    player = new Player();
  }

  void draw() {
    reproductor.shutUp();
    reproductor.gamePlay1Song.play();

    if (reproductor.gamePlay1Song.position() >= 100000) {
      //reproductor.gamePlay1Song.pause();
      reproductor.gamePlay1Song.rewind();
      //reproductor.gamePlay1Song.play();
    }
    if (levelFinishCheck()) {
      reproductor.shutUp();
      reproductor.victorySong.play();
    } else {
      switch (mapx_count) {
      case 1800 :
        counterM += 1;
        break;
      case 16200 :
        counterM += 1;
        break;
      case 6800 :
        //counterM = -1;
        break;
      case 8000 :
        //counterM = -1;
        break;
      case 10200 :
        //counterM = -1;
        break;
      }

      if (mapx_count < duration - 1080) {
        meteorResetCheck(counterM);
      }


      bgDisplay();
      if (mapx_count < 3600) {
        textSize(20);
        fill(255, 255, 255);
        text("Use the UP and DOWN arrow keys to control your acceleration", 30, 30);
        text("You gotta brake before moving to a new direction!", 30, 60);
      }
      if (mapx_count > 1800 && mapx_count < 5400) {
        textSize(20);
        fill(255, 255, 255);
        text("Avoid the other Asteroids!", 2*width/3, height - 30);
      }

      progressText();

      progressbar();
      playerDisplay();
      meteorDisplay(counterM);
      finishAnimation();
      mapUpdate();
      actorCollisionCheck(counterM);
      playerBoundaryCheck();
    }
  }
  void resetLevel() {
    player.resetKeyAccel();
    player.state = 0;
    reproductor.gamePauseSong.rewind();
    reproductor.gamePlay1Song.rewind();
    reproductor.gameOverSong.rewind();
    reproductor.victorySong.rewind();
    map_x1 = 0;
    map_x2 = width + (bg.width - width - 10);
    map_x1 = 0;
    map_x2 = width + (bg.width - width - 10);
    mapx_count = 0;
    counterM = 0;
    player = new Player();
    for (int i = 0; i < NBR_M; i++) {
      m[i] = new Meteorite();
    }
    finished = false;
  }
}