import ddf.minim.*;
import ddf.minim.analysis.*;
import ddf.minim.effects.*;
import ddf.minim.signals.*;
import ddf.minim.spi.*;
import ddf.minim.ugens.*;


boolean start;
int selectedlevel = 1, menu;
boolean pause, gameover, victory;
int volume;
int score;
PFont font;
Level level[];
GameMenu mainmenu;
LevelSelect levelmenu;
Options optionmenu;
GameOver overscreen;
Pause pausescreen;
Credits credit;
// SOUND
Sound reproductor;
Minim min;



void setup() {
  size(1024, 576);
  frameRate(30);
  start = false;
  pause = false;
  gameover = false;
  victory = false;
  mainmenu = new GameMenu();
  font = createFont("Assets/Fonts/DTM-Sans.otf", 50);
  min = new Minim(this);
  textFont(font);
  reproductor = new Sound();
  //Comente las cosas que no estan terminadas para que se puedan hacer pruebas
  levelmenu = new LevelSelect();
  credit = new Credits();
  optionmenu = new Options();
  overscreen = new GameOver();
  pausescreen = new Pause();
  level = new Level[3];
  level[0] = new Level1();
  level[1] = new Level2();
  level[2] = new Level3();
  volume = 100;
  menu = -1;
  selectedlevel = 1;
  //SOUND
}

void draw() {
  //Display de los diferentes menus desde el menu principal

  if (!start) {
    switch (menu) {
    case -1 :
      mainmenu.display();

      break;
    case 1 :
      levelmenu.display();

      break;
    case 2: 
      credit.display();

      break;
    case 3 :
      optionmenu.display();

      break;
    }
  } else {
    //Display de la pantalla de pausa
    if (pause) {
      pausescreen.display();
    } else {
      //Display de la pantalla de victoria
      if (victory) {
        overscreen.displayVictory();
      } else {
        //Display de la pantalla de game over
        if (gameover) {
          overscreen.display();
        } else {
          //Draw y ejecucion del nivel
          level[selectedlevel-1].draw();
        }
      }
    }
  }
}

//Control de teclas
void keyPressed() {
  //Prueba para pasar el control de las teclas a cada menu para separar codigo
  if (!start) {
    switch (menu) {
    case -1 :
      mainmenu.keyPressed();


      break;
    case 1 :
      levelmenu.keyPressed();

      break;
    case 2: 
      credit.keyPressed();

      break;
    case 3 :
      optionmenu.keyPressed();

      break;
    default :
      break;
    }
  } else if (pause) {
    pausescreen.keyPressed();
  } else if (gameover || victory) {
    overscreen.keyPressed();
  } else {
    level[selectedlevel-1].keyPressed();
  }
}

//Control 
void keyReleased() {
  //Prueba para pasar el control de las teclas a cada menu para separar codigo
  if (!start) {
    switch (menu) {
    case -1 :
      mainmenu.keyReleased();
      break;
    case 1 :
      levelmenu.keyReleased();

      break;
    case 2 :
      credit.keyReleased();

      break;
    case 3 :
      optionmenu.keyReleased();
      break;
    default :
      break;
    }
  } else if (pause) {
    pausescreen.keyReleased();
  } else if (gameover || victory) {
    overscreen.keyReleased();
  } else {
    level[selectedlevel-1].keyReleased();
  }
}