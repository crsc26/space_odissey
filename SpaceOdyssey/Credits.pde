public class Credits {
  int selection;
  boolean menukey;
  PImage bg;
  int posy;

  public Credits() {
    bg = loadImage("Assets/Visual/Backgrounds/SpaceBackgroundPixel.jpg");
    selection = 0;
    menukey = true;
  }

  void display() {
    background(bg);
    textSize(70);
    fill(255, 255, 255);
    text("Credits", 270, posy+240);
    fill(255, 255, 255);
    textSize(38);
    text("Arturo Alam Tellez Villagomez A01205569", 50, posy+320);
    fill(255, 255, 255);
    textSize(38);
    text("Christian Ricardo Solís Cortés A0106368", 55, posy+400);
    fill(255, 255, 255);
    textSize(38);
    text("Luis Claudio Soto Ayala A01205935", 120, posy+480);
    fill(255, 255, 255);
    textSize(38);
    text("Rodrigo Reyes Murillo A01064215", 135, posy+560);
    fill(204, 78, 22);
    textSize(40);
    text("Menu", 900, 540);

    textSize(60);
    fill(255, 255, 255);
    text("Music", 320, posy+660);

    textSize(30);
    fill(255, 255, 255);
    text("'Starman' David Bowie (1999), 8 bit version by Tim Furrh", 50, posy+740);
    
    textSize(30);
    fill(255, 255, 255);
    text("'Don't Stop Me Now' Queen (1979) 8 bit version by Jonus04", 50, posy+820);
    
    textSize(30);
    fill(255, 255, 255);
    text("'I'm Very Glad for I'm Coming Home at Last' Eduard Khil", 50, posy+900);
    text("8 bit remix by Robinerd", 50, posy+940);
    
    textSize(30);
    fill(255, 255, 255);
    text("'Star Fox Zero Mission Complete' Nintendo (2016)", 50, posy+1020);
    text("8 bit remix by Bulby", 50, posy+1060);
    
    textSize(25);
    fill(255, 255, 255);
    text("'Life on mars' David Bowie (1971) 8 bit version by Queen Jamie", 50, posy+1140);
    
    textSize(30);
    fill(255, 255, 255);
    text("'Starlight' Muse (2006) 8 bit version by Florio003", 50, posy+1220);
    
    textSize(70);
    fill(255, 255, 255);
    text("Thank you for playing!", 50, posy+1600);
    if (posy > -1500)
      posy -= 2;
  }



  void keyPressed() {

    if (keyCode == ENTER) {
      reproductor.pressEnter.trigger();
      if (selection == 0) {
        menu = -1;
      }
    }
  }

  void keyReleased() {
    if (keyCode == UP) {
      menukey = true;
    }
    if (keyCode == DOWN) {
      menukey = true;
    }
    if (keyCode == ENTER) {
      menukey = true;
    }
  }

  void reset() {
    reproductor.shutUp();
    reproductor.gamePauseSong.rewind();
    reproductor.gamePauseSong.play();
    posy = 0;
  }
}