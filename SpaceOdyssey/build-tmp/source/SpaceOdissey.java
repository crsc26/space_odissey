import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import sprites.utils.*; 
import sprites.maths.*; 
import sprites.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class SpaceOdissey extends PApplet {





StopWatch timer;

boolean start;
int selectedlevel, menu;
boolean pause, gameover, mainstart;
float volume;
PFont font;
//Level level[];
GameMenu mainmenu;
//LevelSelect levelmenu;
//Options optionmenu;
//GameOver overscreen;
//Pause pausescreen;


public void setup() {
	
	start = false;
	pause = false;
	gameover = false;
	mainstart = true;
	mainmenu = new GameMenu();
	font = createFont("Assets/Fonts/DTM-Sans.otf", 50);
    textFont(font);
	//Comente las cosas que no estan terminadas para que se puedan hacer pruebas
	//levelmenu = new LevelSelect();
	//optionmenu = new Options();
	//overscreen = new GameOver();
	/* TODO
	Insertar las clases extendidas de Level (Level 1, 2, 3, etc) dentro del arreglo de niveles
	level[1] = new Level1() ...
	*/
	volume = 100;
	menu = -1;
	selectedlevel = 1;
}

public void initGame() {
	//TODO
	//Aqui se inician/reinicializan variables de los niveles, personaje, etc.
}

public void draw() {
	//Display de los diferentes menus desde el menu principal
	if (mainstart) {
		switch (menu) {
			case -1 :
				mainmenu.display();
			break;
			case 2 :
				//levelmenu.display();
			break;
			case 3 :
				//optionmenu.display();
			break;
			case 0 :
			start = true;
			mainstart = false;
			break;
		}
	} else
	if (start) {
	//Display de la pantalla de pausa
		if (pause) {
			//pausescreen.display();
		} else {
			//Display de la pantalla de game over
			if (gameover) {
				//overscreen.display();
			} else {
				//Draw y ejecucion del nivel
				//level[selectedlevel-1].drawlevel();	
			}
		}
	}
}

//Control de teclas
public void keyPressed() {
	//Prueba para pasar el control de las teclas a cada menu para separar codigo
	if (!start) {
		switch (menu) {
			case -1 :
			mainmenu.keyPressed();
			break;
			case 2 :
			//levelmenu.keyPressed();
			break;
			case 3 :
			//optionmenu.keyPressed();
			break;
			default :
			break;
		}
	}
}

//Control 
public void keyReleased() {
	//Prueba para pasar el control de las teclas a cada menu para separar codigo
		if (!start) {
		switch (menu) {
			case -1 :
			mainmenu.keyReleased();
			break;
			case 2 :
			//levelmenu.keyReleased();
			break;
			case 3 :
			//optionmenu.keyReleased();
			break;
			default :
			break;
		}
	}
}
public class GameMenu {
	int selection;
	boolean menukey;
	PImage bg;

	public GameMenu () {
		bg = loadImage("Assets/Visual/Backgrounds/SpaceBackgroundPixel.jpg");
		selection = 0;
		menukey = true;
	}

	public void display() {
		background(bg);
		textSize(100);
		fill(255,255,255);
	    text("Space Odyssey",180,100);
	    if (selection == 0) {
	      fill(204,78,22);
	      textSize(60);
	      text("New Game",370,380);
	      fill(255,255,255);
	      textSize(60);
	      text("Level Selection",300,430);
	      fill(255,255,255);
	      textSize(60);
	      text("Options",370,480);
	      fill(255,255,255);
	      textSize(60);
	      text("Credits",370,530);
	  }
	  if (selection == 1) {
	      fill(255,255,255);
	      textSize(60);
	      text("New Game",370,380);
	      fill(204,78,22);
	      textSize(60);
	      text("Level Selection",300,430);
	      fill(255,255,255);
	      textSize(60);
	      text("Options",370,480);
	      fill(255,255,255);
	      textSize(60);
	      text("Credits",370,530);
	  }
	  if (selection == 2) {
	      fill(255,255,255);
	      textSize(60);
	      text("New Game",370,380);
	      fill(255,255,255);
	      textSize(60);
	      text("Level Selection",300,430);
	      fill(204,78,22);
	      textSize(60);
	      text("Options",370,480);
	      fill(255,255,255);
	      textSize(60);
	      text("Credits",370,530);
	  }
	  if (selection == 3) {
	      fill(255,255,255);
	      textSize(60);
	      text("New Game",370,380);
	      fill(255,255,255);
	      textSize(60);
	      text("Level Selection",300,430);
	      fill(255,255,255);
	      textSize(60);
	      text("Options",370,480);
	      fill(204,78,22);
	      textSize(60);
	      text("Credits",370,530);
	  }
	}

	public void keyPressed() {
		if (menukey) {
			if(keyCode == UP){
				if(selection > 0) {
	              selection -= 1;
	              menukey = false;
            	}
			}

			if(keyCode == DOWN){
				if(selection < 3) {
	              selection += 1;
	              menukey = false;
            	}
			}

			if(keyCode == ENTER) {
				menu = selection;
			}
		}
	}

	public void keyReleased() {
		if(keyCode == UP) {
		    menukey = true;
		}
		if(keyCode == DOWN) {
		    menukey = true;
		}
		if(keyCode == ENTER) {
		    menukey = true;
		}
	}

}


public class LevelSelect {

	public LevelSelect () {
		
	}

}
public class Options {

	public Options () {
		
	}

}

  public void settings() { 	size(1024, 576); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "SpaceOdissey" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
