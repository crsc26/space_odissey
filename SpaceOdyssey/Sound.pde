
public class Sound {

  AudioPlayer gameMenuSong;
  AudioPlayer gamePlay1Song;
  AudioPlayer gamePlay2Song;
  AudioPlayer gamePlay3Song;
  AudioPlayer victorySong;
  AudioPlayer gameOverSong;
  AudioPlayer gamePauseSong;
  AudioSample select;
  AudioSample pressEnter;
  AudioSample crash;
  AudioSample flyaway;


  public Sound() {

    gameMenuSong = min.loadFile("Assets/Sound/Music/starman.mp3");
    gamePlay1Song = min.loadFile("Assets/Sound/Music/starlight.mp3");
    gamePlay2Song = min.loadFile("Assets/Sound/Music/around.mp3");
    gamePlay3Song = min.loadFile("Assets/Sound/Music/trololol.mp3");
    gameOverSong = min.loadFile("Assets/Sound/Music/gameover.mp3");
    gamePauseSong = min.loadFile("Assets/Sound/Music/gamepause.mp3");
    victorySong = min.loadFile("Assets/Sound/Music/victory.wav");
    select = min.loadSample("Assets/Sound/Effects/sel.wav");
    pressEnter = min.loadSample("Assets/Sound/Effects/enter.mp3");
    crash = min.loadSample("Assets/Sound/Effects/crash.wav");
    flyaway = min.loadSample("Assets/Sound/Effects/flyaways.mp3");
  }

  void shutUp() {
    gameMenuSong.pause();
    gamePlay1Song.pause();
    gamePlay2Song.pause();
    gamePlay3Song.pause();
    gameOverSong.pause();
    gamePauseSong.pause();
    victorySong.pause();
  }
  void stop() {
    gameMenuSong.close();
    gamePlay1Song.close();
    gamePlay2Song.close();
    gamePlay3Song.close();
    gameOverSong.close();
    gamePauseSong.close();
    victorySong.close();
    min.stop();
  }

  void setVol(int vol) {
    switch(vol) {
    case 100:
      gameMenuSong.setGain(5);
      gamePlay1Song.setGain(5);
      gamePlay2Song.setGain(5);
      gamePlay3Song.setGain(5);
      gameOverSong.setGain(5);
      gamePauseSong.setGain(5);
      victorySong.setGain(5);
      break;
    case 80:
      gameMenuSong.setGain(-3);
      gamePlay1Song.setGain(-3);
      gamePlay2Song.setGain(-3);
      gamePlay3Song.setGain(-3);
      gameOverSong.setGain(-3);
      gamePauseSong.setGain(-3);
      victorySong.setGain(-3);
      break;
    case 60:
      gameMenuSong.setGain(-5);
      gamePlay1Song.setGain(-5);
      gamePlay2Song.setGain(-5);
      gamePlay3Song.setGain(-5);
      gameOverSong.setGain(-5);
      gamePauseSong.setGain(-5);
      victorySong.setGain(-5);
      break;
    case 40:
      gameMenuSong.setGain(-8);
      gamePlay1Song.setGain(-8);
      gamePlay2Song.setGain(-8);
      gamePlay3Song.setGain(-8);
      gameOverSong.setGain(-8);
      gamePauseSong.setGain(-8);
      victorySong.setGain(-8);
      break;
    case 20:
      unmuteAll();
      gameMenuSong.setGain(-13);
      gamePlay1Song.setGain(-13);
      gamePlay2Song.setGain(-13);
      gamePlay3Song.setGain(-13);
      gameOverSong.setGain(-13);
      gamePauseSong.setGain(-13);
      victorySong.setGain(-13);
      break;
    case 0:
      muteAll();
      break;
    }
  }

  void muteAll() {
    gameMenuSong.mute();
    gamePlay1Song.mute();
    gamePlay2Song.mute();
    gamePlay3Song.mute();
    gameOverSong.mute();
    gamePauseSong.mute();
    victorySong.mute();
  }
  void unmuteAll() {
    gameMenuSong.unmute();
    gamePlay1Song.unmute();
    gamePlay2Song.unmute();
    gamePlay3Song.unmute();
    gameOverSong.unmute();
    gamePauseSong.unmute();
    victorySong.unmute();
  }
}