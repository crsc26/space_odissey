public class Planet {
  float xpos;
  float ypos;
  int type;
  int colors;
  PImage p;
  Ball col;
  Ball grav;

  public Planet(int t) {
    type = t;
    if (type == 1) {
      xpos = width + 200;
      ypos = height-150;
      col = new Ball(xpos+200, ypos+200, 200);
      grav = new Ball(xpos+200, ypos+200, 350);
    } else {
      xpos = width + 200;
      ypos = -250;
      col = new Ball(xpos+200, ypos+200, 200);
      grav = new Ball(xpos+200, ypos+200, 350);
    }
    colors = (int)random(1, 6);
    switch(colors) {
    case 1 :
      p = loadImage("Assets/Visual/Actors/planet.png");
      break;
    case 2 :
      p = loadImage("Assets/Visual/Actors/planet_yell.png");
      break;
    case 3 :
      p = loadImage("Assets/Visual/Actors/planet_green.png");
      break;
    case 4 :
      p = loadImage("Assets/Visual/Actors/planet_red.png");
      break;
    case 5 :
      p = loadImage("Assets/Visual/Actors/planet_pink.png");
      break;
    }
  }

  void display() {
    //stroke(0, 0, 0);
    //fill(246, 182, 86);
    if (type == 1) {
      col.position.set(xpos+200, ypos+200);
      col.display();
      grav.position.set(xpos+200, ypos+200);
      grav.displayStroke();
      image(p, xpos, ypos);
    } else {
      col.position.set(xpos+200, ypos+200);
      col.display();
      grav.position.set(xpos+200, ypos+200);
      grav.displayStroke();
      image(p, xpos, ypos);
    }
  }

  void update(int xspeed) {
    xpos += -xspeed;
  }

  void reset() {
    if (type == 1) {
      xpos = width + 200;
      ypos = height-150;
      col = new Ball(xpos+200, ypos+200, 200);
      grav = new Ball(xpos+200, ypos+200, 350);
    } else {
      xpos = width + 200;
      ypos = -250;
      col = new Ball(xpos+200, ypos+200, 200);
      grav = new Ball(xpos+200, ypos+200, 350);
    }
  }
}