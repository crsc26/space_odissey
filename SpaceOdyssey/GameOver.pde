public class GameOver {
  int selection;
  boolean opkey;
  PImage bg;

  public GameOver () {
    bg = loadImage("Assets/Visual/Backgrounds/SpaceBackgroundPixel.jpg");
    selection = 1;
    opkey = true;
  }

  void display()
  {
    reproductor.shutUp();
    reproductor.gameOverSong.play();
    background(bg);
    textSize(100);
    fill(255, 255, 255);
    text("GameOver", 100, 100);

    if (selection == 0)
    {
      fill(118, 255, 122);
      textSize(60);
      text("Level Completion:  " + score + "%", 100, 200);

      fill(255, 255, 255);
      textSize(60);
      text("Restart", 400, 300);

      fill(255, 255, 255);
      textSize(60);
      text("End Game", 500, 400);

      fill(255, 255, 255);
      text("Level Select", 600, 500);

      fill(255, 255, 255);
      textSize(60);
    }

    if (selection == 1)
    {
      fill(118, 255, 122);
      textSize(60);
      text("Level Completion:  " + score + "%", 100, 200);

      fill(204, 78, 22);
      textSize(60);
      text("Restart", 400, 300);
      if (keyCode==ENTER)
      {
        textSize(40);
        level[selectedlevel-1].resetLevel();
        gameover = false;
      }

      fill(255, 255, 255);
      textSize(60);
      text("End Game", 500, 400);

      fill(255, 255, 255);
      text("Level Select", 600, 500);
      textSize(60);
    }

    if (selection == 2)
    {
      fill(118, 255, 122);
      textSize(60);
      text("Level Completion:  " + score + "%", 100, 200);

      fill(255, 255, 255);
      textSize(60);
      text("Restart", 400, 300);

      fill(204, 78, 22);
      textSize(60);
      text("End Game", 500, 400);
      if (keyCode==ENTER)
      {
        textSize(40);
        menu = -1;
        reproductor.gameMenuSong.rewind();
        gameover = false;
        start = false;
      }

      fill(255, 255, 255);
      textSize(60);
      text("Level Select", 600, 500);
    }

    if (selection == 3)
    {
      fill(118, 255, 122);
      textSize(60);
      text("Level Completion:  " + score + "%", 100, 200);

      fill(255, 255, 255);
      textSize(60);
      text("Restart", 400, 300);

      fill(255, 255, 255);
      textSize(60);
      text("End Game", 500, 400);

      fill(204, 78, 22);
      textSize(60);
      text("Level Select", 600, 500);
      if (keyCode==ENTER)
      {
        reproductor.shutUp();
        reproductor.gameMenuSong.rewind();
        reproductor.gameMenuSong.play();
        textSize(40);
        menu = 1;
        gameover = false;
        start = false;
      }
    }
  }

  void keyPressed()
  {
    if (keyCode == UP)
    {
      if (victory) {
        if (selection > 0)
        {
          selection -= 1;
          opkey = false;
          reproductor.select.trigger();
        }
      } else {
        if (selection > 1)
        {
          selection -= 1;
          opkey = false;
          reproductor.select.trigger();
        }
      }
    }

    if (keyCode == DOWN)
    {
      if (selection < 3)
      {
        selection += 1;
        opkey = false;
        reproductor.select.trigger();
      }
    }
    if (keyCode == ENTER)
    {
      reproductor.pressEnter.trigger();
    }
  }

  void keyReleased()
  {
    if (keyCode == UP)
    {
      opkey = true;
    }

    if (keyCode == DOWN)
    {
      opkey = true;
    }

    if (keyCode == ENTER)
    {
      opkey = true;
    }
  }

  void displayVictory()
  {
    background(bg);
    textSize(100);
    fill(255, 255, 255);
    text("Level Complete!", 100, 100);


    if (selection == 0)
    {
      fill(204, 78, 22);
      textSize(60);
      text("Next Level", 250, 200);

      if (keyCode==ENTER)
      {
        textSize(40);
        if (selectedlevel != 3) {
          selectedlevel += 1;
          level[selectedlevel-1].resetLevel();
          gameover = false;
          victory = false;
        }
      }

      fill(255, 255, 255);
      textSize(60);
      text("Restart", 400, 300);

      fill(255, 255, 255);
      textSize(60);
      text("End Game", 500, 400);

      fill(255, 255, 255);
      text("Level Select", 600, 500);

      fill(255, 255, 255);
      textSize(60);
    }

    if (selection == 1)
    {
      fill(255, 255, 255);
      textSize(60);
      text("Next Level", 250, 200);

      fill(204, 78, 22);
      textSize(60);
      text("Restart", 400, 300);
      if (keyCode==ENTER)
      {
        textSize(40);
        level[selectedlevel-1].resetLevel();
        gameover = false;
        victory = false;
      }

      fill(255, 255, 255);
      textSize(60);
      text("End Game", 500, 400);

      fill(255, 255, 255);
      text("Level Select", 600, 500);
      textSize(60);
    }

    if (selection == 2)
    {
      fill(255, 255, 255);
      textSize(60);
      text("Next Level", 250, 200);

      fill(255, 255, 255);
      textSize(60);
      text("Restart", 400, 300);

      fill(204, 78, 22);
      textSize(60);
      text("End Game", 500, 400);
      if (keyCode==ENTER)
      {
        reproductor.pressEnter.trigger();
        textSize(40);
        menu = -1;
        reproductor.gameMenuSong.rewind();
        victory = false;
        gameover = false;
        start = false;
      }

      fill(255, 255, 255);
      textSize(60);
      text("Level Select", 600, 500);
    }

    if (selection == 3)
    {
      fill(255, 255, 255);
      textSize(60);
      text("Next Level", 250, 200);

      fill(255, 255, 255);
      textSize(60);
      text("Restart", 400, 300);

      fill(255, 255, 255);
      textSize(60);
      text("End Game", 500, 400);

      fill(204, 78, 22);
      textSize(60);
      text("Level Select", 600, 500);
      if (keyCode==ENTER)
      {
        reproductor.shutUp();
        reproductor.gameMenuSong.rewind();
        reproductor.gameMenuSong.play();
        textSize(40);
        menu = 1;
        victory = false;
        gameover = false;
        start = false;
      }
    }
  }
}