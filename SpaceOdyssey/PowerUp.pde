public class PowerUp {
  float xpos;
  float ypos;
  float x_acceleration;
  float y_acceleration;
  float x_velocity;
  float y_velocity;
  int colors;
  PImage pu;
  PImage spu;
  Ball col;
  int type;
  int counter;

  public PowerUp(int n) {
    counter = 0;
    col = new Ball(xpos+30, ypos+30, 30);
    xpos = width;
    ypos = random(height/6, 5*height/6);
    y_acceleration = 0;
    x_acceleration = random(-0.2, -0.1);
    x_velocity = random(-13, -6);
    y_velocity = 0;
    colors = (int)random(1, 5);
    type = n;
    switch(n) {
    case 1:
      pu = loadImage("Assets/Visual/Actors/powerup_sprite.png");
      spu = loadImage("Assets/Visual/Actors/powerup_sprite_s.png");
      break;
    case 2:
      pu = loadImage("Assets/Visual/Actors/powerup_sprite2.png");
      spu = loadImage("Assets/Visual/Actors/powerup_sprite2_s.png");
      break;
    case 3:
      pu = loadImage("Assets/Visual/Actors/powerup_sprite3.png");
      spu = loadImage("Assets/Visual/Actors/powerup_sprite3_s.png");
      break;
    }
  }

  void display() {
    //stroke(0, 0, 0);
    //fill(246, 182, 86);
    col.position.set(xpos+30, ypos+30);
    col.display();
    image(pu, xpos, ypos);
    if (counter == 75);
      y_acceleration = random(-0.4, 0.4);
  }

  void activePowerDisplay() {
    image(spu, 920, 50);
  }

  void update() {
    x_velocity += x_acceleration;
    y_velocity += y_acceleration;
    xpos += x_velocity;
    ypos += y_velocity;
  }

  void powerUpDisappear()
  {
    xpos = -50;
  }

  void reset() {
    counter = 0;
    xpos = random(width, width+30);
    ypos = random(height/6, 5*height/6);
    x_velocity = random(-13, -6);
    x_acceleration = random(-0.2, -0.1);
    y_velocity = 0;
    y_acceleration = 0;
  }
}