class Ball {
  PVector position;

  float r, m;

  Ball(float x, float y, float r_) {
    position = new PVector(x, y);
    r = r_;
    m = r*.1;
  }

  boolean checkCollision(Ball other) {

    // get distances between the balls components
    PVector bVect = PVector.sub(other.position, position);

    // calculate magnitude of the vector separating the balls
    float bVectMag = bVect.mag();

    if (bVectMag < r + other.r) {
      return true;
    } else {
      return false;
    }
  }

  void display() {
    noStroke();
    noFill();
    ellipse(position.x, position.y, r*2, r*2);
  }

  void displayStroke() {
    stroke(255);
    noFill();
    ellipse(position.x, position.y, r*2, r*2);
  }
}