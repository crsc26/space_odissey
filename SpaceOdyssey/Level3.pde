public class Level3 extends Level {
  int NBR_M = 7;
  int NBR_P = 3;
  int NBR_PU = 3;
  Planet[] p;

  public Level3() {
    bg = loadImage("Assets/Visual/Backgrounds/space-bg4.png");
    bg2 = loadImage("Assets/Visual/Backgrounds/space-bg4.png");
    score = 0;
    map_xspeed = 12;
    map_x1 = 0;
    map_x2 = width + (bg.width - width - 10);
    duration = 32400; //1 Minuto y medio
    mapx_count = 0;
    m = new Meteorite[NBR_M];
    p = new Planet[NBR_P];
    p[0] = new Planet(1);
    p[1] = new Planet(1);
    p[2] = new Planet(0);
    for (int i = 0; i < NBR_M; i++) {
      m[i] = new Meteorite();
    }
    player = new Player();
    // Inicio Power Ups
    pu = new PowerUp[NBR_PU];
    pu[0] = new PowerUp(1);
    pu[1] = new PowerUp(2);
    pu[2] = new PowerUp(3);
    // Fin Power Ups
  }

  void draw() {
    reproductor.shutUp();
    reproductor.gamePlay3Song.play();
    if (levelFinishCheck()) {
      reproductor.shutUp();
      reproductor.victorySong.play();
    } else {
      switch (mapx_count) {
      case 1800 :      // A los 5 segundos
        p[0].reset();  // Pone un planeta en cierto punto
        break;
      case 3600 :      // A los 10 segundos
        pu[0].reset();
        counterM += 1; // Incrementa la cantidad de meteoritos en pantalla
        break;
      case 5400 :      // A los 15 segundos
        p[2].reset();
        pu[1].reset();
        counterM += 1;
        break;
      case 7200 :      // A los 20 segundos
        counterM += 1; // Número de meteoritos, más 1.
      case 9000 :      // A los 25 segundos
        p[1].reset();
        counterM += 1;
        break;
      case 10800 :     // A los 30 segundos
        p[0].reset();
        pu[2].reset();
        break;
      case 12600 :     // A los 35 segundos
        counterM += 1;
      case 14400 :     // A los 40 segundos
        pu[1].reset();
        break;
      case 16200 :     // A los 45 segundos
        p[2].reset();
        p[1].reset();
        pu[0].reset();
        break;
      case 19800 :     // A los 55 segundos
        p[0].reset();
        counterM += 1;
        break;
      case 21600 :     // A los 60 segundos
        pu[2].reset();
        break;
      case 23400 :     // A los 65 segundos
        p[0].reset();
        p[2].reset();
        break;
      case 25200 :     // A los 70 segundos
        pu[1].reset();
        break;
      case 27000 :     // A los 75 segundos
        counterM += 1;
      case 28800 :     // A los 80 segundos
        p[1].reset();
        break;
      case 31320 :     // A los 87 segundos
        p[2].reset();
        break;
      }
      finishAnimation();

      if (mapx_count < duration - 1080) {
        meteorResetCheck(counterM);
      }

      bgDisplay();
      if (mapx_count < 3600) {
        textSize(20);
        fill(255, 255, 255);
        text("There are some power ups for you to pick up!", 30, 30);
        text("Not all of them are exactly helpful though.", 30, 60);
      }

      if (mapx_count % 7200 == 0 && mapx_count > 0)
      {
        pu[(int)random(0, 3)].reset();
      }

      progressText();
      progressbar();
      playerDisplay();
      gravCollision(NBR_P, p);
      if (mapx_count >= 1800) {
        p[0].update(map_xspeed);
        p[0].display();
      }
      if (mapx_count >= 5400) {
        p[2].update(map_xspeed);
        p[2].display();
      }
      if (mapx_count >= 12600) {
        p[1].update(map_xspeed);
        p[1].display();
      }

      meteorDisplay(counterM);
      powerupDisplay(NBR_PU);

      if (player.power == 5 || player.power == 6 || player.power == 7) {
        powerUpTime(10, player.power - 5);
      }
      mapUpdate();
      if (player.state != 5)
        actorCollisionCheck(counterM);
      planetCollisionCheck(NBR_P, p);
      powerUpCollision(NBR_PU);
      playerBoundaryCheck();
    }
  }

  void PowerUpResetCheck(int n) {
    //Reset de los power ups al salir de la pantalla
    for (int i = 0; i < n; i++) {
      if (pu[i].xpos < -5 || pu[i].ypos > height+5 || pu[i].ypos < -5) {
        pu[i].reset();
      }
    }
  }

  void resetLevel() {
    reproductor.gamePauseSong.rewind();
    reproductor.gamePlay3Song.rewind();
    reproductor.gameOverSong.rewind();
    reproductor.victorySong.rewind();
    map_x1 = 0;
    map_x2 = width + (bg.width - width - 10);
    map_x1 = 0;
    map_x2 = width + (bg.width - width - 10);
    mapx_count = 0;

    counterM = 0;
    player = new Player();
    for (int i = 0; i < NBR_M; i++) {
      m[i] = new Meteorite();
    }
    pu[0] = new PowerUp(1);
    pu[1] = new PowerUp(2);
    pu[2] = new PowerUp(3);

    p[0] = new Planet(1);
    p[1] = new Planet(1);
    p[2] = new Planet(2);
    finished = false;
  }
}