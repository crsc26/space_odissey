public class Player {
  float xpos;
  float ypos;
  float plusaccel;
  float minusaccel;
  float acceleration;
  float velocity;
  PImage[] p;
  Ball col;
  int state;
  int power;

  public Player() {
    power = 0;
    col = new Ball(xpos+30, ypos+30, 26);
    xpos = 100;
    ypos = height/2;
    plusaccel = -0.6;
    minusaccel = 0.6;
    acceleration = 0;
    velocity = 0;
    state = 0;
    p = new PImage[14];
    p[0] = loadImage("Assets/Visual/Actors/bigplayer.png");
    p[1] = loadImage("Assets/Visual/Actors/bigplayer1.png");
    p[2] = loadImage("Assets/Visual/Actors/bigplayer2.png");
    p[3] = loadImage("Assets/Visual/Actors/bigplayer3.png");
    p[4] = loadImage("Assets/Visual/Actors/bigplayer4.png");
    p[5] = loadImage("Assets/Visual/Actors/bigplayer5.png");
    p[6] = loadImage("Assets/Visual/Actors/bigplayer6.png");
    p[7] = loadImage("Assets/Visual/Actors/bigplayer7.png");
    p[8] = loadImage("Assets/Visual/Actors/bigplayer8.png");
    p[9] = loadImage("Assets/Visual/Actors/bigplayer9.png");
    p[10] = loadImage("Assets/Visual/Actors/bigplayer10.png");
    p[11] = loadImage("Assets/Visual/Actors/bigplayer11.png");
    p[12] = loadImage("Assets/Visual/Actors/bigplayer12.png");
    p[13] = loadImage("Assets/Visual/Actors/bigplayer13.png");
  }

  void display() {
    //stroke(0, 0, 0);
    //fill(255, 255, 255);
    if (power != 6)
      col.position.set(xpos+30, ypos+30);
    else
      col.position.set(xpos+15, ypos+15);
    col.display();
    image(p[state], xpos, ypos); 
  }

  void update() {
    velocity += acceleration;
    ypos += velocity;
    velocity = constrain(velocity, -10, 10);
  }

  void resetKeyAccel() {
    minusaccel = 0.6;
    plusaccel = -0.6;
  }

  void changeSize()
  {
    col = new Ball(xpos+15, ypos+15, 13);
  }

  void revertSize()
  {
    col = new Ball(xpos+30, ypos+30, 26);
  }
}