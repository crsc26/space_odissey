public class Level2 extends Level {
  int NBR_M = 4;
  int NBR_P = 3;
  Planet[] p;

  public Level2() {
    bg = loadImage("Assets/Visual/Backgrounds/space-bg3.png");
    bg2 = loadImage("Assets/Visual/Backgrounds/space-bg3.png");
    score = 0;
    map_xspeed = 12;
    map_x1 = 0;
    map_x2 = width + (bg.width - width - 10);
    duration = 43200; //2 minutos
    mapx_count = 0;
    m = new Meteorite[NBR_M];
    p = new Planet[NBR_P];
    p[0] = new Planet(1);
    p[1] = new Planet(1);
    p[2] = new Planet(0);
    for (int i = 0; i < NBR_M; i++) {
      m[i] = new Meteorite();
    }
    player = new Player();
  }

  void draw() {
    reproductor.shutUp();
    reproductor.gamePlay2Song.play();
    if (levelFinishCheck()) {
      reproductor.shutUp();
      reproductor.victorySong.play();
    } else {
      switch (mapx_count) {
      case 1800 :
        p[0].reset();
        break;
      case 5400 :
        p[2].reset();
        counterM += 1;
        break;
      case 7200 :
        counterM += 1;
      case 12600 :
        p[1].reset();
        break;
      case 12960 :
        p[2].reset();
        break;
      case 19800 :
        p[0].reset();
        break;
      case 20160 :
        p[2].reset();
        break;
      case 20520 :
        p[1].reset();
        break;
      case 27000 :
        p[0].reset();
        counterM += 1;
        break;
      case 30600 :
        p[2].reset();
        break;
      case 34200 :
        counterM += 1;
        break;
      case 37800 :
        p[1].reset();
        break;
      case 38160 :
        p[2].reset();
        break;
      }
      finishAnimation();

      if (mapx_count < duration - 1080) {
        meteorResetCheck(counterM);
      }

      bgDisplay();
      if (mapx_count < 3600) {
        textSize(20);
        fill(255, 255, 255);
        text("Planets will pull you towards them!", 30, 30);
        text("Be careful with your movements around their gravity field!", 30, 60);
      }

      progressText();
      progressbar();
      playerDisplay();
      gravCollision(NBR_P, p);
      if (mapx_count >= 1800) {
        p[0].update(map_xspeed);
        p[0].display();
      }
      if (mapx_count >= 5400) {
        p[2].update(map_xspeed);
        p[2].display();
      }
      if (mapx_count >= 12600) {
        p[1].update(map_xspeed);
        p[1].display();
      }

      meteorDisplay(counterM);

      
      mapUpdate();
      actorCollisionCheck(counterM);
      planetCollisionCheck(NBR_P, p);
      playerBoundaryCheck();
    }
  }
  void resetLevel() {
    player.state = 0;
    reproductor.gamePauseSong.rewind();
    reproductor.gamePlay2Song.rewind();
    reproductor.gameOverSong.rewind();
    reproductor.victorySong.rewind();
    map_x1 = 0;
    map_x2 = width + (bg.width - width - 10);
    map_x1 = 0;
    map_x2 = width + (bg.width - width - 10);
    mapx_count = 0;

    counterM = 0;
    player = new Player();
    for (int i = 0; i < NBR_M; i++) {
      m[i] = new Meteorite();
    }
    p[0] = new Planet(1);
    p[1] = new Planet(1);
    p[2] = new Planet(2);
    finished = false;
  }
}