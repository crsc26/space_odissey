public class Meteorite {
  float xpos;
  float ypos;
  float x_acceleration;
  float y_acceleration;
  float x_velocity;
  float y_velocity;
  int colors;
  PImage m[];
  Ball col;
  int counter;

  public Meteorite() {
    counter = 0;
    col = new Ball(xpos+35, ypos+30, 25);
    xpos = random(width, width+30);
    ypos = random(height/6, 5*height/6);
    y_acceleration = 0;
    x_acceleration = random(-0.2, -0.1);
    x_velocity = random(-13, -6);
    y_velocity = 0;
    m = new PImage[4];
    colors = (int)random(0, 4);
    m[0]= loadImage("Assets/Visual/Actors/bigasteroid_def.png");
    m[1] = loadImage("Assets/Visual/Actors/bigasteroid_blue.png");
    m[2] = loadImage("Assets/Visual/Actors/bigasteroid_green.png");
    m[3] = loadImage("Assets/Visual/Actors/bigasteroid_red.png");
  }

  void display() {
    //stroke(0, 0, 0);
    //fill(246, 182, 86);
    col.position.set(xpos+35, ypos+30);
    col.display();
    image(m[colors], xpos, ypos);
    counter += 1;
    if (counter == 75);
    y_acceleration = random(-0.4, 0.4);
  }

  void update() {
    x_velocity += x_acceleration;
    y_velocity += y_acceleration;
    xpos += x_velocity;
    ypos += y_velocity;
  }

  void reset() {
    counter = 0;
    colors = (int)random(0, 4);
    xpos = random(width, width+30);
    ypos = random(height/6, 5*height/6);
    x_velocity = random(-13, -6);
    x_acceleration = random(-0.2, -0.1);
    y_velocity = 0;
    y_acceleration = 0;
  }
}