public class GameMenu {
  int selection;
  boolean menukey;
  PImage bg;

  public GameMenu () {
    bg = loadImage("Assets/Visual/Backgrounds/SpaceBackgroundPixel.jpg");
    selection = 0;
    menukey = true;
  }

  void display() {
    reproductor.shutUp();
    reproductor.gameMenuSong.play();
    background(bg);
    textSize(100);
    fill(255, 255, 255);
    text("Space Odyssey", 180, 100);
    if (selection == 0) {
      fill(204, 78, 22);
      textSize(60);
      text("New Game", 370, 280);
      fill(255, 255, 255);
      textSize(60);
      text("Level Selection", 270, 360);
      fill(255, 255, 255);
      textSize(60);
      text("Credits", 380, 440);
      fill(255, 255, 255);
      textSize(60);
      text("Options", 380, 520);
    }
    if (selection == 1) {
      fill(255, 255, 255);
      textSize(60);
      text("New Game", 370, 280);
      fill(204, 78, 22);
      textSize(60);
      text("Level Selection", 270, 360);
      fill(255, 255, 255);
      textSize(60);
      text("Credits", 380, 440);
      fill(255, 255, 255);
      textSize(60);
      text("Options", 380, 520);
    }
    if (selection == 2) {
      fill(255, 255, 255);
      textSize(60);
      text("New Game", 370, 280);
      fill(255, 255, 255);
      textSize(60);
      text("Level Selection", 270, 360);
      fill(204, 78, 22);
      textSize(60);
      text("Credits", 380, 440);
      fill(255, 255, 255);
      textSize(60);
      text("Options", 380, 520);
    }
    if (selection == 3) {
      fill(255, 255, 255);
      textSize(60);
      text("New Game", 370, 280);
      fill(255, 255, 255);
      textSize(60);
      text("Level Selection", 270, 360);
      fill(255, 255, 255);
      textSize(60);
      text("Credits", 380, 440);
      fill(204, 78, 22);
      textSize(60);
      text("Options", 380, 520);
    }
  }

  void keyPressed() {
    if (menukey) {
      if (keyCode == UP) {
        if (selection > 0) {
          selection -= 1;
          menukey = false;
          reproductor.select.trigger();
        }
      }

      if (keyCode == DOWN) {
        if (selection < 3) {
          selection += 1;
          menukey = false;
          reproductor.select.trigger();
        }
      }

      if (keyCode == ENTER) {
        reproductor.pressEnter.trigger();
        if (selection == 0) {
          start = true;
          selectedlevel = 1;
          level[selectedlevel-1].resetLevel();
        }
        if (selection == 1) {
          menu = 1;
        }
        if (selection == 2) {
          credit.reset();
          menu = 2;
        }
        if (selection == 3) {
          menu = 3;
          optionmenu.selection = 0;
        }
      }
    }
  }

  void keyReleased() {
    if (keyCode == UP) {
      menukey = true;
    }
    if (keyCode == DOWN) {
      menukey = true;
    }
    if (keyCode == ENTER) {
      menukey = true;
    }
  }
}